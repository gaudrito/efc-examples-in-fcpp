// Copyright © 2020 Giorgio Audrito. All Rights Reserved.

/**
 * @file examples.hpp
 * @brief Implementation of the case study comparing collection algorithms.
 */

#ifndef EXAMPLES_H_
#define EXAMPLES_H_

#include <limits>

#include "lib/beautify.hpp"
#include "lib/coordination/utils.hpp"


/**
 * @brief Namespace containing all the objects in the FCPP library.
 */
namespace fcpp {


//! @brief Namespace containing the libraries of coordination routines.
namespace coordination {


namespace tags {
    //! @brief The areaLog value reaching the gateway from the anomaly.
    struct gateway_log {};
}


//! @brief Counts the number of messages received by each neighbour.
FUN() field<int> uniConnection(ARGS) { CODE
    return old(CALL, field<int>{0}, [&](field<int> o){
        return o + mod_other(CALL, 1, 0);
    });
}

//! @brief Counts the number of bidirectional communications with each neighbour.
FUN() field<int> biConnection(ARGS) { CODE
    return nbr(CALL, field<int>{0}, [&](field<int> n){
        return n + mod_other(CALL, 1, 0);
    });
}

//! @brief Returns the number of hops to the closest device where `source` is true.
FUN() double distanceTo(ARGS, bool source) { CODE
    return nbr(CALL, INF, [&](field<double> n){
        return mux(source, 0.0, min_hood(CALL, n, INF)+1.0);
    });
}

//! @brief Propagates value along ascending dist.
FUN(T) T broadcast(ARGS, double dist, T value) { CODE
    tuple<double,T> loc{dist, value};
    return get<1>(nbr(CALL, loc, [&](field<tuple<double,T>> n){
        return make_tuple(dist, get<1>(min_hood(CALL, n, loc)));
    }));
}

//! @brief Block C - converge-cast.
FUN(F, G) double C(ARGS, bool sink, double value, F&& accumulate, G&& divide) { CODE
    double dist = distanceTo(CALL, sink);
    return get<1>(nbr(CALL, make_tuple(dist,value), [&](field<tuple<double,double>> n){
        field<int> conn = mux(get<0>(n) < dist, biConnection(CALL), 0);
        field<double> send = conn / (double)max(sum_hood(CALL, conn, 0), 1);
        field<double> recv = nbr(CALL, send);
        return make_tuple(dist, fold_hood(CALL, accumulate, map_hood(divide, get<1>(n), recv), value));
    }));
}

//! @brief Returns the number of hops between the closests source and dest devices.
FUN() int distance(ARGS, bool source, bool dest) { CODE
    return broadcast(CALL, distanceTo(CALL, source), distanceTo(CALL, dest));
}

//! @brief Returns true on the way between the closest source and dest devices, false elsewhere
FUN() bool channel(ARGS, bool source, bool dest, double width) { CODE
    return distanceTo(CALL, source) + distanceTo(CALL, dest) <= distance(CALL, source, dest) + width;
}

//! @brief Broadcasts the value in source along the channel between source and dest
FUN(T) T broadcastChannel(ARGS, bool source, bool dest, double width, T value) { CODE
    return channel(CALL, source, dest, width) ? broadcast(CALL, distanceTo(CALL, source), value) : value;
}

//! @brief Sample code calling the above functions.
FUN() void example(ARGS) { CODE
    bool gateway = node.uid == 0; // gateway is node 0
    // anomaly is in node 1 between times 25 and 50
    bool anomaly = node.uid == 1 and 25 < node.current_time() and node.current_time() < 50;
    double log = 1; // dummy log value (areaLog counts the area size)

    double areaLog = distanceTo(CALL, anomaly) < 5 ? C(CALL, anomaly, log, [](double x, double y){
        return x+y;
    }, [](double x, double y){
        return x*y;
    }) : 0;
    double result = broadcastChannel(CALL, anomaly, gateway, 10, areaLog);
    node.storage(tags::gateway_log{}) = gateway ? result : 0;
}


}


//! @brief Main struct calling `example`.
MAIN(coordination::example,);


}

#endif // EXAMPLES_H_
