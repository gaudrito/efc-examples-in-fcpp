// Copyright © 2020 Giorgio Audrito. All Rights Reserved.

#include "lib/fcpp.hpp"

#include "lib/examples.hpp"

using namespace fcpp;
using namespace component::tags;
using namespace coordination::tags;

#define DEVICE_NUM  1000
#define END_TIME    100
#define MAXX        2000
#define MAXY        200

using spawn_s = sequence::multiple_n<DEVICE_NUM, 0>;

using round_s = sequence::periodic<distribution::interval_n<times_t, 0, 1>, distribution::weibull_n<times_t, 100, 25, 100>, distribution::constant_n<times_t, END_TIME+2>>;

using export_s = sequence::periodic_n<1, 0, 1, END_TIME>;

using rectangle_d = distribution::rect_n<1, 0, 0, MAXX, MAXY>;

DECLARE_OPTIONS(opt,
    program<main>,
    retain<metric::retain<2>>,
    round_schedule<round_s>,
    exports<
        double, field<int>, field<double>, tuple<double,double>, tuple<field<double>,double>
    >,
    log_schedule<export_s>,
    aggregators<
        gateway_log,    aggregator::sum<double>
    >,
    tuple_store<
        gateway_log,    double
    >,
    spawn_schedule<spawn_s>,
    init<
        x,          rectangle_d
    >,
    connector<connect::fixed<100>>
);

int main() {
    batch::run(component::batch_simulator<opt>{});
    return 0;
}
