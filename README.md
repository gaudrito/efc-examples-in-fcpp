# EFC Examples in FCPP
## _The Edge-Field Calculus_


This repository provides a working translation of the examples in _The Edge-Field Calculus_ (submitted to ESOP 2021) into the FCPP programming language. The source code can be inspected in `lib/examples.hpp`.

The first version of the FCPP language, **without** support for _Edge-Field Calculus_ (EFC) constructs, has been presented at [ACSOS 2020 - 1st IEEE International Conference on Autonomic Computing and Self-Organizing Systems](https://conf.researchr.org/home/acsos-2020) through [this paper](http://giorgio.audrito.info/static/fcpp.pdf). The FCPP version used in this repository has been extended to implement the novel EFC constructs, through the `field<T>` class. FCPP code still supports usage of non-field values of type `T`, which are interpreted as constant maps.

The examples implemented compute the sum of a *log* value within a radius of 5 of an *anomaly* node, sending the result through a channel to a *gateway* node. Two random nodes are selected as *gateway* and *anomaly* nodes, where the anomaly node only manifests an anomaly between times *25s* and *50s*.  A sample *log* value constantly equal to 1 is assumed, so that the sum of logs corresponds to the size of the area of radius 5 close to the anomaly (which is about 200 nodes out of the 1000 total). Nodes fire every *1s* (with a 25% random error), have a connection radius of *100m*, and are randomly displaced into a *2000m* x *200m* rectangular area. 

The program outputs the value that reaches the gateway every second. The desired value (about 200) is reached shortly after the anomaly manifests (at time *35s* with a *10s* delay), and then is dropped shortly after the anomaly disappears (at time *65s* with a *15s* delay).
For any issues with reproducing the experiments, please contact [Giorgio Audrito](mailto:giorgio.audrito@unito.it).

## Getting Started

### References

- FCPP main website: [https://fcpp.github.io](https://fcpp.github.io).
- FCPP documentation: [http://fcpp-doc.surge.sh](http://fcpp-doc.surge.sh).
- FCPP sources: [https://github.com/fcpp/fcpp](https://github.com/fcpp/fcpp).

### Vagrant

Download Vagrant from [https://www.vagrantup.com](https://www.vagrantup.com), then type the following commands in a terminal:
```
vagrant up
vagrant ssh
cd fcpp
./make.sh run -O runner
```
Then you should get output about building the experiments and running them (in the Vagrant virtual machine). After that you can exit and stop the virtual machine through:
```
exit
vagrant halt
```

### Docker

Download Docker from [https://www.docker.com](https://www.docker.com), then download the Docker container from GitHub by typing the following command in a terminal:
```
docker pull docker.pkg.github.com/fcpp/fcpp/container:1.0
```
Alternatively, you can build the container yourself with the following command:
```
docker build -t docker.pkg.github.com/fcpp/fcpp/container:1.0 .
```
Once you have the Docker container locally available, type the following commands:
```
docker run -it --volume $PWD:/fcpp --workdir /fcpp docker.pkg.github.com/fcpp/fcpp/container:1.0 bash
./make.sh run -O runner
```
Then you should get output about building the experiments and running them (in the Docker container). After that you can exit and stop the container through:
```
exit
```

### Custom Build

In order to get started on your machine you need the following installed:

- [Bazel](https://bazel.build) (tested with version 2.1.0)
- [GCC](https://gcc.gnu.org) (tested with version 9.3.0)

Once you have them installed, you should be able to run `./make.sh gcc run -O runner`, getting output about building the experiments and running them.
